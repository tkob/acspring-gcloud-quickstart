Atlassian Connect Spring Boot on Google Cloud Quick Start
=========================================================

This Maven archetype helps you to develop a GAE-hosted, Atlassian Connect Spring Boot-based JIRA add-on.
The add-on application runs on App Engine and use Cloud Datastore as storage.
You only need the free tier if your add-on is not large.

Getting Started
---------------

Be sure you have installed latest Google Cloud SDK and have set up default project.

Add the following to your setting.xml, which is located at ~/.m2/::

    <repositories>
        <repository>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
            <id>bintray-tkob-maven</id>
            <name>bintray</name>
            <url>http://dl.bintray.com/tkob/maven</url>
        </repository>
    </repositories>

Generate your project from archetype::

    mvn archetype:generate -DarchetypeGroupId=yokohama.unit \
    -DarchetypeArtifactId=acspring-gcloud-archetype -DarchetypeVersion=0.1.0

Move to the created project directory::

    cd YOUR-PROJECT-DIR

Open src/main/resources/application.yml and replace addon.base-url property with your appspot URL::

    addon:
      key: acspring-gcp
      base-url: https://YOUR-GCP-PROJECT-ID.appspot.com

Create indexes for Cloud Datastore (used by the sample application)::

    gcloud app deploy index.yaml

Then deploy the application to App Engine::

    mvn -Pappengine appengine:deploy

Install the add-on to your JIRA site. The sample application fetches and stores issues and displays summary.
