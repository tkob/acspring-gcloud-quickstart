package yokohama.unit.acspring_gcp.service;

import java.util.Arrays;

import lombok.Value;

import org.junit.Assert;
import org.junit.Test;

import yokohama.unit.acspring_gcp.model.DataTable;

public class XyAggregatorTest {
    @Value
    static class Person {
        String name;
    }

    @Value
    static class Type {
        String label;
    }

    @Value
    static class Issue {
        Person assignee;
        Type type;
    }

    @Test
    public void testXyAggregate() {
        // Setup
        XyAggregator aggregator = new XyAggregator();
        Issue[] data = {
            new Issue(new Person("John"), new Type("Task")),
            new Issue(new Person("John"), new Type("Task")),
            new Issue(new Person("John"), new Type("Bug")),
            new Issue(new Person("Mary"), new Type("Task")),
        };

        // Exercise
        DataTable dataTable = aggregator.aggregate(
            Arrays.asList(data),
            Arrays.asList("type", "label"), "nolabel",
            Arrays.asList("assignee", "name"), "noname",
            "Assignee");

        // Verify
        Assert.assertEquals(
            new DataTable(
                Arrays.asList("Assignee", "Task", "Bug"),
                Arrays.asList(
                    Arrays.asList("John", 2, 1),
                    Arrays.asList("Mary", 1, 0))),
            dataTable);
    }
}
