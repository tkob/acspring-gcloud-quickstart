package yokohama.unit.acspring_gcp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.gclouddatastore.repository.EnableGcloudDatastoreRepositories;

@SpringBootApplication
@EnableGcloudDatastoreRepositories
public class AddonApplication {

    public static void main(String[] args) throws Exception {
        new SpringApplication(AddonApplication.class).run(args);
    }
}
