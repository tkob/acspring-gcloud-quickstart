package yokohama.unit.acspring_gcp;

import java.io.IOException;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.gclouddatastore.repository.Context;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.PathElement;
import com.google.cloud.datastore.Query;

import yokohama.unit.acspring_gcp.jira.model.Issue;
import yokohama.unit.acspring_gcp.jira.model.Worklog;
import yokohama.unit.acspring_gcp.jira.repository.IssueRepository;
import yokohama.unit.acspring_gcp.jira.repository.WorklogRepository;
import yokohama.unit.acspring_gcp.jira.service.SearchService;
import yokohama.unit.acspring_gcp.jira.service.UserService;
import yokohama.unit.acspring_gcp.jira.service.WorklogChangedSinceService;
import yokohama.unit.acspring_gcp.jira.service.WorklogListService;
import yokohama.unit.acspring_gcp.model.DataTable;
import yokohama.unit.acspring_gcp.service.XyAggregator;

@Controller
public class IframeController {
    private static final Logger log = LoggerFactory.getLogger(IframeController.class);

    @Autowired
    AtlassianHostRepository atlassianHostRepository;

    @Autowired
    UserService userService;

    @Autowired
    SearchService searchService;

    @Autowired
    WorklogChangedSinceService worklogChangedSinceService;

    @Autowired
    WorklogListService worklogListService;

    @Autowired
    IssueRepository issueRepository;

    @Autowired
    WorklogRepository worklogRepository;

    @Autowired
    XyAggregator xyAggregator;

    @GetMapping("/iframe")
    public String getIframe(@AuthenticationPrincipal AtlassianHostUser hostUser,
            Model model) throws IOException {

        // Get AtlassianHost
        String hostBaseUrl = hostUser.getHost().getBaseUrl();
        AtlassianHost atlassianHost = atlassianHostRepository
                .findFirstByBaseUrl(hostBaseUrl)
                .orElseThrow(() -> new RuntimeException("unknown host: " + hostBaseUrl));

        // Get timestamp of last updated issue from the datastore
        Optional<Issue> lastUpdatedIssue = getLastUpdatedIssue(atlassianHost);
        Instant lastUpdated = lastUpdatedIssue
                .flatMap(issue -> {
                    Object updated = issue.getFields().get("updated");
                    log.debug("updated=" + updated + " " + updated.getClass());
                    return (updated != null && updated instanceof Instant)
                            ? Optional.of((Instant) updated) : Optional.empty();
                    })
                .orElse(Instant.EPOCH);
        log.debug("lastUpdated=" + lastUpdated);

        // Retrieve recently updated issues from JIRA
        String jql = String.format(
            "updated >= '%s'",
            lastUpdated
                .atZone(ZoneId.of(userService.getMyself().getTimeZone()))
                .format(DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm")));
        log.debug("Searching by JQL {}", jql);
        Iterable<Issue> issues = searchService.searchIssues(jql);

        try (Context ctx = Context.with(PathElement.of("AtlassianHost", atlassianHost.getClientKey()))) {
            // Save newly retrieved issues to the datastore
            issueRepository.save(issues);

            // Calculate statistics
            DataTable data = xyAggregator.aggregate(
                    issueRepository.findAll(),
                    Arrays.asList("fields", "issuetype", "name"), "Unknown",
                    Arrays.asList("fields", "assignee", "name"), "No One",
                    "Assignee");
            model.addAttribute("data", data);

            // Get timestamp of last updated worklog from the datastore
            Instant since = worklogRepository.findFirstByOrderByUpdatedDesc()
                    .map(worklog -> OffsetDateTime
                            .parse(worklog.getUpdated(),
                                    DateTimeFormatter
                                            .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSxxxx"))
                            .toInstant())
                    .orElse(Instant.EPOCH);
            log.debug("worklog changed since: " + since + " " + since.toEpochMilli());

            // Retrieve and save recently changed worklogs
            Iterable<Worklog> worklogs = worklogListService.getWorklogs(
                    worklogChangedSinceService.getUpdated(since.toEpochMilli()));
            worklogRepository.save(worklogs);
         }
        return "iframe";
    }

    private Optional<Issue> getLastUpdatedIssue(AtlassianHost atlassianHost) {
        Query<Entity> query = Query
            .newGqlQueryBuilder(
                Query.ResultType.ENTITY,
                "select * from Issue where __key__ has ancestor key(AtlassianHost, '" +
                        atlassianHost.getClientKey() +
                        "') order by fields.updated desc limit 1")
            .setAllowLiteral(true)
            .build();
        Iterator<Issue> issueIter = issueRepository.query(query).iterator();

        return issueIter.hasNext() ? Optional.of(issueIter.next()) : Optional.empty();
      }
}
