package yokohama.unit.acspring_gcp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.stereotype.Service;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import lombok.Getter;
import yokohama.unit.acspring_gcp.model.DataTable;

@Service
public class XyAggregator {
    static class Accessor {
        @Getter
        Object object;
        BeanWrapper beanWrapper;

        public Accessor(Object object) {
            this.object = object;
            this.beanWrapper = PropertyAccessorFactory
                    .forBeanPropertyAccess(object);
        }

        @SuppressWarnings("unchecked")
        public Object get(String name) {
            if (object instanceof Map) {
                return ((Map<String, Object>)object).get(name);
            }
            else {
                return beanWrapper.getPropertyValue(name);
            }
        }
    }

    Optional<?> navigate(Object entity, Iterable<String> path) {
        Accessor accessor = new Accessor(entity);
        for (String pathElement : path) {
            Object value = accessor.get(pathElement);
            if (value == null) {
                return Optional.empty();
            }
            else {
                accessor = new Accessor(value);
            }
        }
        return Optional.of(accessor.getObject());
    }

    public DataTable aggregate(Iterable<?> entities,
        Iterable<String> xAxis, Object xDefault,
        Iterable<String> yAxis, Object yDefault,
        String yName) {

        Table<Object, Object, Integer> table = HashBasedTable.create();
        for (Object entity : entities) {
            Optional<?> xValueOpt = navigate(entity, xAxis);
            Object xValue = xValueOpt.isPresent() ? xValueOpt.get() : xDefault;
            Optional<?> yValueOpt = navigate(entity, yAxis);
            Object yValue = yValueOpt.isPresent() ? yValueOpt.get() : yDefault;
            if (table.contains(yValue, xValue)) {
                table.put(yValue, xValue, table.get(yValue, xValue) + 1);
            }
            else {
                table.put(yValue, xValue, 1);
            }
        }

        List<?> columnKeys = table.columnKeySet().stream()
                .collect(Collectors.toList());
        List<String> columns = new ArrayList<>();
        columns.add(yName);
        columns.addAll(columnKeys.stream().map(Objects::toString)
                .collect(Collectors.toList()));

        List<List<Object>> rows = new ArrayList<>();
        for (Object rowKey : table.rowKeySet()) {
            List<Object> row = new ArrayList<>();
            row.add(rowKey);
            for (Object columnKey : columnKeys) {
                Integer count = table.get(rowKey, columnKey);
                if (count == null) {
                    row.add(0);
                }
                else {
                    row.add(count);
                }
            }
            rows.add(row);
        }
        return new DataTable(columns, rows);
    }
}
