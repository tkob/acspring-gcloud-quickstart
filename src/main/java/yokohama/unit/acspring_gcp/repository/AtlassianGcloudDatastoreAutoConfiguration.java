package yokohama.unit.acspring_gcp.repository;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.gclouddatastore.repository.EnableGcloudDatastoreRepositories;

import com.atlassian.connect.spring.AtlassianHostRepository;

@Configuration
@EnableGcloudDatastoreRepositories(basePackageClasses = { AtlassianHostRepository.class })
public class AtlassianGcloudDatastoreAutoConfiguration {
}