
package yokohama.unit.acspring_gcp.jira.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Worklog Change
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "worklogId", "updatedTime", "properties" })
@Data
@AllArgsConstructor
public class WorklogChange {
    @JsonProperty("worklogId")
    private int worklogId;
    @JsonProperty("updatedTime")
    private long updatedTime;
    @JsonProperty("properties")
    private List<EntityProperty> properties = new ArrayList<EntityProperty>();
}
