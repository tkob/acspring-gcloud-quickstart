
package yokohama.unit.acspring_gcp.jira.model;

import java.net.URI;
import java.util.Map;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "self", "key", "accountId", "name", "emailAddress", "avatarUrls",
        "displayName", "active", "timeZone", "locale", "groups", "applicationRoles",
        "expand" })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @JsonProperty("self")
    private URI self;
    @JsonProperty("key")
    private String key;
    @Id
    @JsonProperty("accountId")
    private String accountId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("emailAddress")
    private String emailAddress;
    @JsonProperty("avatarUrls")
    private Map<String, String> avatarUrls;
    @JsonProperty("displayName")
    private String displayName;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("active")
    private boolean active;
    @JsonProperty("timeZone")
    private String timeZone;
    @JsonProperty("locale")
    private String locale;
    /**
     * Simple List Wrapper
     * <p>
     * 
     * 
     */
    @JsonProperty("groups")
    private Map<String, Object> groups;
    /**
     * Simple List Wrapper
     * <p>
     * 
     * 
     */
    @JsonProperty("applicationRoles")
    private Map<String, Object> applicationRoles;
    @JsonProperty("expand")
    private String expand;
}
