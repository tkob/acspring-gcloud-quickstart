
package yokohama.unit.acspring_gcp.jira.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Search Results
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "expand", "startAt", "maxResults", "total", "issues",
        "warningMessages" })
@Data
@AllArgsConstructor
public class SearchResults {
    @JsonProperty("expand")
    private String expand;

    @JsonProperty("startAt")
    private long startAt;

    @JsonProperty("maxResults")
    private long maxResults;

    @JsonProperty("total")
    private long total;

    @JsonProperty("issues")
    private List<Issue> issues;

    @JsonProperty("warningMessages")
    private List<String> warningMessages;
}
