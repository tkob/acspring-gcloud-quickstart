
package yokohama.unit.acspring_gcp.jira.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Worklog Changed Since
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "values", "since", "until", "isLastPage", "self", "nextPage" })
@Data
@AllArgsConstructor
public class WorklogChangedSince {

    @JsonProperty("values")
    private List<WorklogChange> values = new ArrayList<WorklogChange>();
    @JsonProperty("since")
    private long since;
    @JsonProperty("until")
    private long until;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("isLastPage")
    private boolean isLastPage;
    @JsonProperty("self")
    private URI self;
    @JsonProperty("nextPage")
    private URI nextPage;
}
