
package yokohama.unit.acspring_gcp.jira.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Worklog
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "self", "author", "updateAuthor", "comment", "created", "updated",
        "visibility", "started", "timeSpent", "timeSpentSeconds", "id", "issueId",
        "properties" })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Worklog {

    @JsonProperty("self")
    private URI self;
    /**
     * User
     * <p>
     * 
     * 
     */
    @JsonProperty("author")
    private User author;
    /**
     * User
     * <p>
     * 
     * 
     */
    @JsonProperty("updateAuthor")
    private User updateAuthor;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty("created")
    private String created;
    @JsonProperty("updated")
    private String updated;
    /**
     * Visibility
     * <p>
     * 
     * 
     */
    @JsonProperty("visibility")
    private Visibility visibility;
    @JsonProperty("started")
    private String started;
    @JsonProperty("timeSpent")
    private String timeSpent;
    @JsonProperty("timeSpentSeconds")
    private int timeSpentSeconds;
    @Id
    @JsonProperty("id")
    private String id;
    @JsonProperty("issueId")
    private String issueId;
    @JsonProperty("properties")
    private List<EntityProperty> properties = new ArrayList<EntityProperty>();
}
