package yokohama.unit.acspring_gcp.jira.model;

import java.util.Map;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "key" })
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Issue implements Comparable<Issue> {
    @Id
    @JsonProperty("id")
    private String id;

    @JsonProperty("key")
    private String key;

    @JsonProperty("fields")
    private Map<String, Object> fields;

    @Override
    public int compareTo(@NonNull Issue o) {
        return Integer.getInteger(id).compareTo(Integer.getInteger(o.id));
    }
}
