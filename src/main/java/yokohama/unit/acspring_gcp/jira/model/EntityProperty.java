
package yokohama.unit.acspring_gcp.jira.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Entity Property
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "key", "value" })
@Data
@AllArgsConstructor
public class EntityProperty {
    @JsonProperty("key")
    private String key;
    @JsonProperty("value")
    private Object value;
}
