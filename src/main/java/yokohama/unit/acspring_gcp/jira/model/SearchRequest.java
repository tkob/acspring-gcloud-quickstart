
package yokohama.unit.acspring_gcp.jira.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Search Request
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "jql", "startAt", "maxResults", "fields", "validateQuery",
        "properties", "fieldsByKeys" })
@Data
@AllArgsConstructor
public class SearchRequest {

    @JsonProperty("jql")
    private String jql;
    @JsonProperty("startAt")
    private long startAt;
    @JsonProperty("maxResults")
    private long maxResults;
    @JsonProperty("fields")
    private List<String> fields;
    @JsonProperty("validateQuery")
    private String validateQuery;
    @JsonProperty("properties")
    private List<String> properties;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("fieldsByKeys")
    private boolean fieldsByKeys;
}
