
package yokohama.unit.acspring_gcp.jira.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Visibility
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "type", "value" })
@Data
@AllArgsConstructor
public class Visibility {
    @JsonProperty("type")
    private Visibility.Type type;
    @JsonProperty("value")
    private String value;

    public enum Type {

        GROUP("group"), ROLE("role");
        private final String value;
        private final static Map<String, Visibility.Type> CONSTANTS = new HashMap<String, Visibility.Type>();

        static {
            for (Visibility.Type c : values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static Visibility.Type fromValue(String value) {
            Visibility.Type constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            }
            else {
                return constant;
            }
        }
    }
}
