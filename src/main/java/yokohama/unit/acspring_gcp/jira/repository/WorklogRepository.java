package yokohama.unit.acspring_gcp.jira.repository;

import java.util.Optional;

import org.springframework.data.gclouddatastore.repository.GcloudDatastoreRepository;

import yokohama.unit.acspring_gcp.jira.model.Worklog;

public interface WorklogRepository extends GcloudDatastoreRepository<Worklog, String>{
    Optional<Worklog> findFirstByOrderByUpdatedDesc();
}
