package yokohama.unit.acspring_gcp.jira.repository;

import org.springframework.data.gclouddatastore.repository.GcloudDatastoreRepository;

import yokohama.unit.acspring_gcp.jira.model.Issue;

public interface IssueRepository extends GcloudDatastoreRepository<Issue, String>{
}
