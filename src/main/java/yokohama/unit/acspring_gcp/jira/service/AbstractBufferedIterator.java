package yokohama.unit.acspring_gcp.jira.service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class AbstractBufferedIterator<E> implements Iterator<E> {

    private final Queue<E> agenda = new LinkedList<E>();

    protected long startAt = 0;

    abstract protected long refill(long startAt, Queue<E> agenda);

    abstract protected E transform(E e);

    boolean fetch() {
        startAt = refill(startAt, agenda);
        return startAt == -1;
    }

    @Override
    public boolean hasNext() {
        return !agenda.isEmpty() || startAt >= 0;
    }

    @Override
    public E next() {
        E next = agenda.poll();
        if (next != null) {
            return transform(next);
        }

        fetch();
        if (agenda.isEmpty()) {
            throw new NoSuchElementException();
        }

        return transform(agenda.poll());
    }
}
