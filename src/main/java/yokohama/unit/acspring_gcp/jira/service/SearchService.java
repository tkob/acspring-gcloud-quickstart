package yokohama.unit.acspring_gcp.jira.service;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRestClients;

import lombok.RequiredArgsConstructor;
import yokohama.unit.acspring_gcp.jira.model.Issue;
import yokohama.unit.acspring_gcp.jira.model.SearchRequest;
import yokohama.unit.acspring_gcp.jira.model.SearchResults;

@Service
public class SearchService {
    private static final Logger log = LoggerFactory.getLogger(SearchService.class);

    private static final int MAX_RESULTS = 50;

    @Autowired
    AtlassianHostRestClients atlassianHostRestClients;

    @RequiredArgsConstructor
    public static class IssueIterator extends AbstractBufferedIterator<Issue> {
        private final RestTemplate restTemplate;
        private final String jql;

        @Override
        protected long refill(long startAt, Queue<Issue> agenda) {
            SearchRequest searchRequest = new SearchRequest(jql, startAt, MAX_RESULTS,
                    null, "true", null, false);
            SearchResults searchResults = restTemplate.postForObject("/rest/api/2/search",
                    searchRequest, SearchResults.class);
            List<String> warningMessages = searchResults.getWarningMessages();
            if (warningMessages != null) {
                for (String warningMessage : warningMessages) {
                    log.warn("Warning from JIRA REST API: {0}", warningMessage);
                }
            }
            List<Issue> issues = searchResults.getIssues();
            agenda.addAll(issues);
            long totalSoFar = startAt + issues.size();
            if (totalSoFar < searchResults.getTotal()) {
                return totalSoFar;
            }
            else {
                return -1;
            }
        }

        @Override
        protected Issue transform(Issue issue) {
            convertTimestamp(issue.getFields());
            return issue;
        }

        private static List<String> possiblyDateTime =
            Arrays.asList("created", "updated", "resolutiondate");

        private static final DateTimeFormatter dateTimeFormatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSxxxx");

        @SuppressWarnings("unchecked")
        void convertTimestamp(Map<String, Object> map) {
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if (value instanceof String && possiblyDateTime.contains(key)) {
                    try {
                        OffsetDateTime dateTime =
                            OffsetDateTime.parse(
                                (String)value, dateTimeFormatter);
                        map.put(entry.getKey(), dateTime);
                        log.debug("convert " + value + " to " + dateTime);
                    } catch (DateTimeParseException e) {
                        log.debug((String)value, e);
                        continue;
                    }
                }
                else if (value instanceof Map) {
                    convertTimestamp((Map<String, Object>)value);
                }
            }
        }
    }

    @RequiredArgsConstructor
    public static class IssueIterable implements Iterable<Issue> {
        private final RestTemplate restTemplate;
        private final String jql;

        @Override
        public Iterator<Issue> iterator() {
            IssueIterator iter = new IssueIterator(restTemplate, jql);
            iter.fetch();
            return iter;
        }
    }

    public Iterable<Issue> searchIssues(String jql) {
        RestTemplate restTemplate = atlassianHostRestClients.authenticatedAsAddon();

        return new IssueIterable(restTemplate, jql);
    }

    public List<Issue> searchIssuesList(String jql) {
        List<Issue> issues = new ArrayList<Issue>();
        searchIssues(jql).forEach(issue -> issues.add(issue));
        return issues;
    }
}
