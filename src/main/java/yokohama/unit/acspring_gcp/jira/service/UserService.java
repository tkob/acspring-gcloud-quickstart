package yokohama.unit.acspring_gcp.jira.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRestClients;

import yokohama.unit.acspring_gcp.jira.model.User;

@Service
public class UserService {
    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    AtlassianHostRestClients atlassianHostRestClients;

    public User getMyself() {
        RestTemplate restTemplate = atlassianHostRestClients.authenticatedAsAddon();
        User myself = restTemplate.getForObject("/rest/api/2/myself", User.class);
        return myself;
    }

}
