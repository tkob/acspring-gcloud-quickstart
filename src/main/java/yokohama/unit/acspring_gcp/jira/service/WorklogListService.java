package yokohama.unit.acspring_gcp.jira.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.stream.StreamSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRestClients;

import lombok.RequiredArgsConstructor;
import yokohama.unit.acspring_gcp.jira.model.Worklog;
import yokohama.unit.acspring_gcp.jira.model.WorklogChange;
import yokohama.unit.acspring_gcp.jira.model.WorklogIdsRequest;

@Service
public class WorklogListService {
    private static final Logger log = LoggerFactory.getLogger(WorklogListService.class);

    @Autowired
    AtlassianHostRestClients atlassianHostRestClients;

    @RequiredArgsConstructor
    public static class WorklogIterator extends AbstractBufferedIterator<Worklog> {
        private final RestTemplate restTemplate;
        private final Iterator<Integer> idIterator;

        private static final int REQUEST_LIMIT = 10000;
        private static final int AGENDA_LIMIT = 50;

        @Override
        protected long refill(long startAt, Queue<Worklog> agenda) {
            List<Integer> ids = new ArrayList<Integer>();
            while (idIterator.hasNext() && ids.size() < REQUEST_LIMIT
                    && ids.size() < AGENDA_LIMIT) {
                int id = idIterator.next();
                ids.add(id);
            }
            WorklogIdsRequest worklogIdsRequest = new WorklogIdsRequest(ids);
            Worklog[] worklogs = restTemplate.postForObject("/rest/api/2/worklog/list",
                    worklogIdsRequest, Worklog[].class);

            agenda.addAll(Arrays.asList(worklogs));

            return idIterator.hasNext() ? 0 : -1;
        }

        @Override
        protected Worklog transform(Worklog worklog) {
            return worklog;
        }
    }

    @RequiredArgsConstructor
    public static class WorklogIterable implements Iterable<Worklog> {
        private final RestTemplate restTemplate;
        private final Iterable<WorklogChange> worklogChanges;

        @Override
        public Iterator<Worklog> iterator() {
            Iterator<Integer> idIterator = StreamSupport
                    .stream(worklogChanges.spliterator(), false)
                    .map(WorklogChange::getWorklogId).iterator();

            WorklogIterator iter = new WorklogIterator(restTemplate, idIterator);
            iter.fetch();
            return iter;
        }
    }

    public Iterable<Worklog> getWorklogs(Iterable<WorklogChange> worklogChanges) {
        RestTemplate restTemplate = atlassianHostRestClients.authenticatedAsAddon();
        return new WorklogIterable(restTemplate, worklogChanges);
    }

}
