package yokohama.unit.acspring_gcp.jira.service;

import java.util.Iterator;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRestClients;

import lombok.RequiredArgsConstructor;
import yokohama.unit.acspring_gcp.jira.model.WorklogChange;
import yokohama.unit.acspring_gcp.jira.model.WorklogChangedSince;

@Service
public class WorklogChangedSinceService {
    private static final Logger log = LoggerFactory
            .getLogger(WorklogChangedSinceService.class);

    @Autowired
    AtlassianHostRestClients atlassianHostRestClients;

    @RequiredArgsConstructor
    public static class WorklogChangedSinceIterator
            extends AbstractBufferedIterator<WorklogChange> {

        private final RestTemplate restTemplate;
        private final String restApiUrl;

        public WorklogChangedSinceIterator(RestTemplate restTemplate, String restApiUrl,
                long since) {
            this.restTemplate = restTemplate;
            this.restApiUrl = restApiUrl;
            this.startAt = since;
        }

        @Override
        protected long refill(long startAt, Queue<WorklogChange> agenda) {
            WorklogChangedSince worklogChangedSince = restTemplate
                    .getForObject(restApiUrl, WorklogChangedSince.class, startAt);

            List<WorklogChange> values = worklogChangedSince.getValues();
            agenda.addAll(values);

            log.debug(worklogChangedSince.toString());
            if (worklogChangedSince.isLastPage()) {
                return -1;
            }
            else {
                return worklogChangedSince.getUntil();
            }
        }

        @Override
        protected WorklogChange transform(WorklogChange worklogChange) {
            return worklogChange;
        }
    }

    @RequiredArgsConstructor
    public static class WorklogChangedSinceIterable implements Iterable<WorklogChange> {
        private final RestTemplate restTemplate;
        private final String restApiUrl;
        private final long since;

        @Override
        public Iterator<WorklogChange> iterator() {
            WorklogChangedSinceIterator iter = new WorklogChangedSinceIterator(
                    restTemplate, restApiUrl, since);
            iter.fetch();
            return iter;
        }
    }

    public Iterable<WorklogChange> getDeleted(long since) {
        RestTemplate restTemplate = atlassianHostRestClients.authenticatedAsAddon();
        return new WorklogChangedSinceIterable(restTemplate,
                "/rest/api/2/worklog/deleted?since={since}", since);
    }

    public Iterable<WorklogChange> getUpdated(long since) {
        RestTemplate restTemplate = atlassianHostRestClients.authenticatedAsAddon();
        return new WorklogChangedSinceIterable(restTemplate,
                "/rest/api/2/worklog/updated?since={since}", since);
    }
}
