all:
	mvn clean archetype:create-from-project
	rm -rf target/generated-sources/archetype/target/
	rm -f target/generated-sources/archetype/src/main/resources/archetype-resources/Makefile
	rm -f target/generated-sources/archetype/src/main/resources/archetype-resources/README.rst
	rm -f target/generated-sources/archetype/src/main/resources/archetype-resources/pom-archetype.xml
	perl -i -pne 's/yokohama.unit.acspring_gcp/\$${package}/g' target/generated-sources/archetype/src/main/resources/archetype-resources/pom.xml
	cp pom-archetype.xml target/generated-sources/archetype/pom.xml
	(cd target/generated-sources/archetype; mvn package install)
